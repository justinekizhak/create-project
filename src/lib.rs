//! Create your projects
//! 
//! # Table of contents
//! - [Introduction](#introduction)
//! - [How?](#how)
//! - [Why?](#why)
//! - [Features](#features)
//! - [Getting Started](#getting-started)
//!   - [Installation](#installation)
//!   - [Usage](#usage)
//! - [Full Documentation](#full-documentation)
//! - [License](#license)
//! 
//! # Introduction
//! 
//! `create` is a command line application for creating and scaffolding
//!  your software project.   
//! You can create all kinds of projects from HTML CSS JS projects to 
//! Rust, Python, C/C++ etc.
//! 
//! **[Back to top](#table-of-contents)**
//! 
//! # How?
//! 
//! Everything begins with a git template repo. You use your own or 
//! someone else's git template repo. It can be hosted anywhere Github,
//! Gitlab, Bitbucket or self-hosted.
//! 
//! `create` downloads the repo's latest commit and removes `.git` 
//! folder within it.
//! 
//! **[Back to top](#table-of-contents)**
//! 
//! # Why?
//! 
//! TBH I wanted to learn Rust and what's better way to learn a language
//! by creating a command-line application.
//! 
//! Also I wanted to create a Rust version of [degit] which I love.
//! 
//! ## So... Is this a Rust clone of degit?
//! 
//! No. Although it tries to implement degit's git repo download 
//! mechanism, but this one tries to do more than that like creating
//! a Github/Gitlab repo on your behalf for the project you are creating.
//! 
//! Maybe autogenerate a README for the project, add some labels to the
//! Github project list, create a Github project board etc. 
//! 
//! Basically do all kinds of boilerplate work for creating a new project.  
//! 
//! **[Back to top](#table-of-contents)**
//! 
//! # Features
//! 
//! - Everything is customizable from a yaml config file.
//! - Pre and post `create` hooks. 
//! - Supports bash commands like pipes, grep etc
//! 
//! **[Back to top](#table-of-contents)**
//! 
//! # Getting started
//! 
//! ## Installation
//! 
//! Install the binary
//! 
//! ## Usage
//! 
//! ```bash
//! create
//! ```    
//! Thats it? Yep.    
//! Run `create` in your "project home" folder. `create` is a command-line 
//! wizard, so just follow the instructions. 
//!
//! [degit]: https://github.com/rich-harris/degit
//! 
//! **[Back to top](#table-of-contents)**
//! 
//! # Full Documentation
//! 
//! For more help run `create -h` or `create --help` on terminal.
//! 
//! Any feature requests, bugs etc feel free to create an issue on [Gitlab][website].
//! 
//! Read [CHANGELOG], [CODE OF CONDUCT], [CONTRIBUTING] guide.
//! 
//! [website]: https://gitlab.com/justinekizhak/create-project
//! [CHANGELOG]: CHANGELOG.md
//! [CODE OF CONDUCT]: CODE_OF_CONDUCT.md
//! [CONTRIBUTING]: CONTRIBUTING.md
//! 
//! **[Back to top](#table-of-contents)**
//! 
//! # License
//! 
//! [MIT License]
//! 
//! Copyright (c) 2019 Justine Thomas Kizhakkinedath
//! 
//! [MIT License]: LICENSE.txt
//! 
//! **[Back to top](#table-of-contents)**
//! 

/// This module contains functions related to IO for files as well as
/// interacting with users.
/// 
/// Inspired by Haskell's separation of impure IO functions by a keyword,
/// this does this by putting it inside a `io` module. That way we know
/// if any function that can create side-effects is inside this module.
/// 
/// # Layout
/// 
/// Split into 
/// - file
/// - user
/// 
/// `file` submodule contains read/write for files.
/// 
/// `user` submodule contains functions for user interactions.
pub mod io;

/// This module contains the struct `Preset` and functions related.
/// 
/// Presets are the building blocks of this app. 
/// All projects are created from git template repo, which are called
/// "preset".
/// Whenever you run `create` it will display all inbuilt preset and
/// any other preset you have configured. And you get to select from it.
pub mod preset;
