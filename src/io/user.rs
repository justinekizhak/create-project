use std::io;
use std::io::Write;
use crate::preset::Preset;

use dialoguer::{theme::ColorfulTheme, Select};

pub struct ProjectInfo {
    pub name: String,
    pub description: String,
}

// ----------Public functions----------------------------------------

/// Prints the tooltip on the prompt and takes input from the user and
/// returns as `String`
/// 
/// # Examples
/// 
/// ```no_run
/// use create::io::user::input;
/// 
/// let user_input = input("Please enter something");
/// ```
pub fn input(tooltip: &str) -> String {
    print!("{}: ", tooltip);
    io::stdout().flush().unwrap();
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");
    input.trim().to_string()
}

/// Adds new preset into existing preset vector
/// 
/// # Examples
/// 
/// ```no_run
/// use create::io::user::create_new_preset;
/// use create::preset::Preset;
/// 
/// let input = vec![Preset{
/// name: "Python".to_string(), 
/// url: "https://www.python.org".to_string()
/// }];
/// 
/// let output = create_new_preset(input);
/// ```
pub fn create_new_preset(mut input: Vec<Preset>) -> Vec<Preset> {
    input.push(new_preset());
    input
}

/// Gets the project info from the user
/// 
/// Asks the user for the project details like name, description etc
/// returns `ProjectInfo`.
/// 
/// # Examples
/// 
/// ```rust
/// use create::io::user::project_info;
/// 
/// let user_input = project_info();
/// 
/// println!("Project name: {}", user_input.name);
/// println!("Project description: {}", user_input.description);
/// ```
pub fn project_info() -> ProjectInfo {
    ProjectInfo{
        name: input("Please enter the name of the project"),
        description: input("Please enter the description of the project"),
    }
}

/// TODO look into display trait for printing
pub fn print_project_info(input: ProjectInfo) {
    println!("Project name: {:?}", input.name);
    println!("Project description: {:?}", input.description);
}

/// Takes in a `&[String]` array or a `&Vec<String>` as input and `&str`
/// tooltip and returns the name of the selection
/// 
/// # Examples
/// 
/// ```no_run
/// use create::io::user::select_input;
/// 
/// let input: Vec<String> = ["Foo", "bar", "Baz"].iter().map(|x| x.to_string()).collect();
/// 
/// let user_selected = select_input(&input, "Please select an option");
/// println!("You have selected: {}", user_selected);
/// ```
pub fn select_input(selections: &[String], tooltip: &str) -> String {
    let selection = Select::with_theme(&ColorfulTheme::default())
        .with_prompt(tooltip)
        .default(0)
        .items(&selections)
        .interact_opt()
        .unwrap();

    return_preset_name(selections, selection)
}

// ----------Private functions---------------------------------------

/// PRIVATE FUNCTION: Take input from user to create a new preset and 
/// returns it
/// 
/// Use allowed only in this module.
/// 
/// # Examples
/// 
/// ```compile_fail
/// let new_preset_from_user = new_preset();
/// ```
fn new_preset() -> Preset {
    Preset {
        name : input("Please enter new preset name"),
        url : input("Please enter new preset url"),
    }
}

/// PRIVATE FUNCTION: Used by `select_preset` function. 
/// 
/// The `select_preset` function returns only the index of the selected
/// choice along with the selections vector.  
/// This function takes the index and returns the `String` in the index.
/// 
/// Use allowed only in this module.
/// 
/// # Examples
/// 
/// ```compile_fail
/// let selected_preset_name = return_preset_name(input_vector, index_of_selected);
/// ```
fn return_preset_name(input_vector: &[String], selected: Option<usize>) -> String{
    let return_value = match selected {
        Some(x) => input_vector[x].as_str(),
        None => panic!("INTERNAL ERROR: None selected"),
    };
    String::from(return_value)
}

