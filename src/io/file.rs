use std::fs::File;
use std::io::prelude::*;
use std::error::Error;
use std::path::Path;

// ----------Public functions----------------------------------------

/// Reads and returns file contents
/// 
/// This function takes `&str` as input file path, read its contents and returns a 
/// `String`.
/// 
/// # Examples
/// ```no_run
/// use create::io::file::read;
/// 
/// let file_contents = read("/path/to/file");
/// ```
pub fn read(file_path: &str) -> String{
    let path = Path::new(file_path);
    let display = path.display();
    let mut file = match File::open(&path) {
        Ok(file) => file,
        Err(why) => panic!("INTERNAL ERROR: File doesn't exists {}: {}", display, 
        why.description()),
    };
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("INTERNAL ERROR: Couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => s,
    }
}

/// Writes a `String` into a file
/// 
/// This function takes `&str` as file path and `&str` as file contents
/// and writes.
/// 
/// # Examples
/// ```no_run
/// use create::io::file::write;
/// 
/// let file_contents = write("/path/to/file", "This is the contents of the file");
/// ```
pub fn write(file_path: &str, file_contents: &str) -> File{
    let path = Path::new(file_path);
    let display = path.display();
    let mut file = match File::create(&path) {
        Err(why) => panic!("INTERNAL ERROR: Couldn't create {}: {}", display, why.description()),
        Ok(file) => file,
    };
    match file.write_all(file_contents.as_bytes()) {
        Err(why) => panic!("INTERNAL ERROR: Couldn't write to {}: {}", display, why.description()),
        Ok(_) => println!("Successfully wrote to {}", display),
    }
    file
}
