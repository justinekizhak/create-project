#[macro_use]
extern crate clap;
extern crate create;

use clap::App;
use clap::AppSettings;

static CONFIG_DIR: &'static str = "$HOME./create";
static PRESET_DIR: &'static str = "src/presets.yaml";

struct UserOptions {
    label: &'static str,
    id: &'static str,
}

fn main() {
    let user_options: Vec<UserOptions> = vec![
        UserOptions {
            label: "Scaffold a new local project",
            id: "create",
        },
        UserOptions {
            label: "Add a new preset",
            id: "add",
        },
        UserOptions {
            label: "Create a GitHub project and upload the current directory",
            id: "createGH",
        },
        UserOptions {
            label: "Create a GitLab project and upload the current directory",
            id: "createGL",
        },
    ];

    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml)
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();

    match matches.subcommand() {
        ("clone", Some(sub_m)) => {
            println!("Subcommand clone args: {:?}", sub_m.args);
            // TODO
        }
        ("pull", Some(sub_m)) => {
            println!("Subcommand pull args: {:?}", sub_m.args);
            // TODO
        }
        ("wizard", _) => {
            println!("Booting up the wizard...");
            wizard_step2(wizard_step1(user_options));
        }
        _ => panic!("INTERNAL ERROR: Error matching subcommand"),
    }
    println!("All done");
}

fn wizard_step1(user_options: Vec<UserOptions>) -> &'static str {
    fn return_id(user_ans: String, user_options: Vec<UserOptions>) -> &'static str {
        for option in &user_options {
            if user_ans == option.label {
                return option.id;
            }
        }
        panic!("INTERNAL ERROR: Miss match between user ans and displayed options")
    }

    let selections: Vec<String> = user_options.iter().map(|x| x.label.to_string()).collect();
    let user_ans = create::io::user::select_input(&selections, "Hi, what do you want to do today?");
    return_id(user_ans, user_options)
}

fn wizard_step2(input: &'static str) {
    fn add_preset(presets: Vec<create::preset::Preset>, file_path: &str) {
        let presets = create::io::user::create_new_preset(presets);
        create::preset::write_presets(file_path, presets);
    }

    match input {
        "create" => {
            println!("Creating new project...");
            create::io::user::project_info();
            let presets = create::preset::read_presets(PRESET_DIR);
            create::preset::select_preset(&presets);
        }
        "add" => {
            println!("Adding a new preset...");
            let presets = create::preset::read_presets(PRESET_DIR);
            add_preset(presets, PRESET_DIR);
        }
        "createGH" => {
            println!("Creating a new project on GitHub using the current directory...");
            // TODO
        }
        "createGL" => {
            println!("Creating a new project on GitLab using the current directory...");
            // TODO
        }
        _ => {
            panic!("INTERNAL ERROR: Failure to match USER SELECTION with ID");
        }
    }
}
