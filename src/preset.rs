use crate::io::file;
use crate::io::user;
use std::path::Path;

use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Preset {
    pub name: String,
    pub url: String,
}

// ----------Public functions----------------------------------------

/// Converts(deserialize) yaml `String` to a `Vec<Preset>`
/// 
/// # Examples
/// 
/// ```rust
/// use create::preset::deserialize;
/// use create::preset::Preset;
/// 
/// let input = "---\n- name: Python\n  url: https://www.python.org";
/// let expected_output = vec![Preset{
/// name: "Python".to_string(), 
/// url: "https://www.python.org".to_string()
/// }];
/// 
/// let output = deserialize(input.to_string());
/// assert_eq!(output[0].name, expected_output[0].name);
/// assert_eq!(output[0].url, expected_output[0].url);
/// ```
pub fn deserialize(data: String) -> Vec<Preset> {
    match serde_yaml::from_str(&data) {
        Err(why) => panic!("INTERNAL ERROR: {}", why),
        Ok(data) => data,
    }
}

/// Converts(serialize) `Vec<Preset>` into yaml `String`
/// 
/// # Examples
/// 
/// ```rust
/// use create::preset::serialize;
/// use create::preset::Preset;
/// 
/// let expected_output = "---\n- name: Python\n  url: \"https://www.python.org\"";
/// let input = vec![Preset{
/// name: "Python".to_string(), 
/// url: "https://www.python.org".to_string()
/// }];
/// 
/// let output = serialize(input);
/// assert_eq!(output, expected_output);
/// ```
pub fn serialize(data: Vec<Preset>) -> String {
    match serde_yaml::to_string(&data) {
        Err(why) => panic!("INTERNAL ERROR: {}", why),
        Ok(data) => data,
    }
}

/// Takes `Vec<Preset>` and returns vector of the names 
/// of all the presets
/// 
/// `Preset` contains names as well as url. This function takes all
/// names and creates another vector with just the names and returns it.
/// 
/// # Examples
/// 
/// ```rust
/// use create::preset::get_preset_names;
/// use create::preset::Preset;
/// 
/// let expected_output = vec!["Python".to_string()];
/// let input = vec![Preset{
/// name: "Python".to_string(), 
/// url: "https://www.python.org".to_string()
/// }];
/// 
/// let output = get_preset_names(&input);
/// assert_eq!(output, expected_output);
/// ```
pub fn get_preset_names(presets: &[Preset]) -> Vec<String> {
    let mut preset_names: Vec<String> = vec![];
    for preset in presets {
        preset_names.push(preset.name.to_string());
    }
    preset_names
}

/// Take a file path and returns `Vec<Preset>`
/// 
/// # Examples
/// 
/// ```no_run
/// use create::preset::read_presets;
/// 
/// let presets = read_presets("/file/path.yaml");
/// ```
pub fn read_presets(file_path: &str) -> Vec<Preset> {
    println!("Reading presets from file {}...", file_path);
    if !Path::new(file_path).exists() {
        let contents = "---
- name: name_of_preset
  url: preset_git_url";
        file::write(file_path, contents);
    }
    let contents = file::read(file_path);
    deserialize(contents)
}

/// Take a file path and `Vec<Preset>` and writes to
/// disk
/// 
/// # Examples
/// 
/// ```no_run
/// use create::preset::Preset;
/// use create::preset::write_presets;
/// 
/// let preset_vector = vec![Preset{
/// name: "Python".to_string(), 
/// url: "https://www.python.org".to_string()
/// }];
/// 
/// let presets = write_presets("/file/path.yaml", preset_vector);
/// ```
pub fn write_presets(file_path: &str, data: Vec<Preset>) {
    let serialized_data = serialize(data);
    file::write(file_path, &serialized_data);
}

/// Take a reference to `Vec<Preset>` as primary input 
/// options and reference to `Vec<String>` as secondary options and 
/// returns a `String`
/// 
/// Both inputs will be taken and merged into one vector and displayed
/// to user.
/// 
/// # Examples
/// 
/// ```no_run
/// use create::preset::Preset;
/// use create::preset::select_preset;
/// 
/// let primary_input = vec![Preset{
/// name: "Python".to_string(), 
/// url: "https://www.python.org".to_string()
/// }];
/// 
/// let secondary_input = vec![
/// "Go back to previous menu".to_string(), 
/// "Go back to the main menu".to_string()];
/// 
/// let presets = select_preset(&primary_input);
/// ```
pub fn select_preset(input: &[Preset]) -> String{
    let preset_names: Vec<String> = get_preset_names(input);
    user::select_input(&preset_names, "Pick your preset: ")
}