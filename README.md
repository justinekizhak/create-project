<a name="top"></a>
[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)
<a href="https://www.instagram.com/justinekizhak"><img src="https://i.imgur.com/G9YJUZI.png" alt="Instagram" align="right"></a>
<a href="https://twitter.com/justinekizhak"><img src="http://i.imgur.com/tXSoThF.png" alt="Twitter" align="right"></a>
<a href="https://www.facebook.com/justinekizhak"><img src="http://i.imgur.com/P3YfQoD.png" alt="Facebook" align="right"></a>
<br>

- - -

<!-- {Put your badges here} -->

- - -

# Create project <!-- omit in toc -->

- - -

## Table of contents <!-- omit in toc -->

- [Introduction](#introduction)
- [Features](#features)
- [Getting Started](#getting-started)
- [Full Documentation](#full-documentation)
- [License](#license)

## Introduction

  *create_project* will help you in scaffolding a new project, create your project on Github, Gitlab.
  It can be used to create any project, be it Python, ReactJS, SvelteJS, Rust etc.

**[Back to top](#table-of-contents)**

## Features

**[Back to top](#table-of-contents)**

## Getting Started

Run `cargo run` to run in dev mode
`cargo test` for testing
`cargo doc --open` to open docs.

**[Back to top](#table-of-contents)**

## Full Documentation

For full documentation [read the docs]()

Visit [website]().

Read [CHANGELOG], [CODE OF CONDUCT], [CONTRIBUTING] guide.

[CHANGELOG]: CHANGELOG.md
[CODE OF CONDUCT]: CODE_OF_CONDUCT.md
[CONTRIBUTING]: CONTRIBUTING.md

## License

**[Back to top](#table-of-contents)**

- - -

[![forthebadge](https://forthebadge.com/images/badges/built-with-swag.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-with-resentment.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)

- - -
